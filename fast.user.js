// ==UserScript==
// @name           Fast search GUI
// @namespace      https://bitbucket.org/majun/fast-user-js
// @description    Add wysiwyg editor and remove on Enter key press
// @version        0.3
// @updateURL      https://bitbucket.org/majun/fast-user-js/raw/master/fast.user.js
// @copyright      2013+, Martin Junger
// @include        http://fesrchjd*
//   wysiwyg CodeMirror
// @require        http://codemirror.net/lib/codemirror.js
// @resource       cssCm http://codemirror.net/lib/codemirror.css
//   syntax
// @require        http://codemirror.net/mode/sql/sql.js
//
// @run-at         document-end
// ==/UserScript==

// define textarea
var area = document.getElementById('query');

// width, height
var areaWidth = document.body.clientWidth - 50;
var areaHeight = 300;
area.style.width = areaWidth;
area.style.height = areaHeight;

// TODO Ctrl+Enter to textarea
//area.onkeypress = function(event) { if ((event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey) { document.forms[0].submit(); } };

// Wysiwyg
// load styles
GM_addStyle(GM_getResourceText('cssCm'));
// border
var customCSS = GM_addStyle('.CodeMirror { border: 1px solid black; line-height: 1.3em; }');
GM_addStyle(customCSS);

// insert editor
var sqleditor;
function initCM() {
	sqleditor = CodeMirror.fromTextArea(area, {
		mode: 'text/x-sql',
		indentUnit: 4,
		indentWithTabs: false,
		smartIndent: true,
		lineNumbers: true,
		matchBrackets : true,
		autofocus: true
	});
	sqleditor.on('change', function () {
		var content = sqleditor.getValue();
		area.value = content;
	});
	sqleditor.setSize(areaWidth+'px', areaHeight+'px');
};
initCM();
